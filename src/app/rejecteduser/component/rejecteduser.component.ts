import { Component, ViewEncapsulation, ViewContainerRef, AfterViewInit, AfterContentInit } from '@angular/core';
import { AppConfig } from "../../app.config";
import { Router } from '@angular/router';

//importing service
import { RejectedUserService } from '../../rejecteduser/rejecteduser.service';

import { Modal } from 'angular2-modal/plugins/bootstrap';

import { ModalModule } from "ng2-modal";
//=================== importing form components ==================
import { FormGroup, FormBuilder, Validators, AbstractControl, FormControl } from '@angular/forms';
declare var swal: any;
import { PagesComponent } from '../../pages/pages.component';

@Component({
    selector: 'rejecteduser',
    encapsulation: ViewEncapsulation.None,
    styleUrls: ['./rejecteduser.component.scss'],
    templateUrl: './rejecteduser.component.html',
    providers: [RejectedUserService]
})

export class RejectedUserComponent {
    rejectedUser: any;
    userReactive = [];
    public rowsOnPage = 10;
    reuser: any;
    public p = 1;
    public count: any;
    public totalPages: any;
    public searchEnabled = 0;
    public searchTerm = '';
    msg: any = false;
    detailData:any;
    constructor(private _appConfig: AppConfig, private _rejectedUserService: RejectedUserService, private router: Router, vcRef: ViewContainerRef, fb: FormBuilder,public _isAdmin: PagesComponent) { }

    ngOnInit() {
        if (this._isAdmin.isAdmin == false) {
            var role = sessionStorage.getItem('role');
            var roleDt = JSON.parse(role);
            for (var x in roleDt) {
                if (x == 'registered-users') {
                    if (roleDt[x] == 0) {
                        this.router.navigate(['error']);
                    } else if(roleDt[x] == 100){
                        jQuery("#btnreactive").hide();
                    } else if(roleDt[x] == 110){
                        jQuery("#btnreactive").hide();
                    }
                }
            }
        }
        this.p = 1;
        this.getPage(this.p);
    }
    getPage(p) {
        this._rejectedUserService.rejecteduser(p - 1, this.rowsOnPage, this.searchEnabled, this.searchTerm, ).subscribe(
            result => {
                if (result.data && result.data.length > 0) {
                    this.msg = false;
                    this.rejectedUser = result.data;
                    this.p = p;
                    this.count = result.count;
                } else {
                    this.rejectedUser = [];
                    this.msg = "No Rejected Users Available";
                }
            }
        )
    }
    getPageOnSearch(term) {
        this.searchTerm = term;
        if (this.searchTerm.length > 0) {
            this.searchEnabled = 1;
        } else {
            this.searchEnabled = 0;
        }
        this.getPage(this.p);

    }
    gotocheck(username, event) {
        if (event.target.checked) {
            this.userReactive.push(username);
        } else if (!event.target.checked) {
            let indexx = this.userReactive.indexOf(username);
            this.userReactive.splice(indexx, 1);
        }
        if (this.userReactive.length == 0) {
        }

    }
    gotoReactiveUser() {
        if (this.userReactive.length == 0) {
            swal("Please Select User");
        } else {
            this._rejectedUserService.reactiveuser(this.userReactive).subscribe(
                result => {
                    this.reuser = result;
                    swal("Good job!", "User Re-Activated Successfully", "success");
                    this.ngOnInit();
                }
            )
        }
    }
    gotoUserDatail(user) {
        this._rejectedUserService.getUserDetail(user).subscribe(
            result => {
                if (result.code == 200) {
                    this.detailData = result.data;
                    jQuery('#userDetail').modal('show');
                }
            }
        )
    }


}