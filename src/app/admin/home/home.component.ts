import { Component, ViewEncapsulation, ViewContainerRef, AfterViewInit, AfterContentInit } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { AppConfig } from "../../app.config";
import { Modal } from 'angular2-modal/plugins/bootstrap';
import { ModalModule } from "ng2-modal";
import { FormGroup, FormBuilder, Validators, AbstractControl, FormControl } from '@angular/forms';
import { HomeService } from './home.service';


declare var swal: any;
declare var sweetAlert: any;
@Component({
    selector: 'home',
    encapsulation: ViewEncapsulation.None,
    styleUrls: ['./home.component.scss'],
    templateUrl: './home.component.html',
    providers: [HomeService]
})

export class HomeComponent {
    seoSetting: FormGroup;
    socialMedia: FormGroup;
    keyWord: any;
    data: any;
    title: any;
    description: any;
    constructor(private route: ActivatedRoute, private _service: HomeService, private router: Router, vcRef: ViewContainerRef, fb: FormBuilder) {
        this.seoSetting = fb.group({
            'title': ['', Validators.required],
            'description': ['', Validators.required],
            // 'keyword': ['', Validators.required],
        });
        // this.socialMedia = fb.group({
        //     'image':['',Validators.required],
        //     'title' : ['',Validators.required],
        //     ''
        // })
    }
    ngOnInit() {
        var type = 1;
        this._service.getSEOSetting(type).subscribe(
            res => {
                // console.log("res", res);
                if (res.data && res.data.length > 0) {
                    this.title = res.data.title;
                    this.description = res.data.description;
                    this.data = res.data.keyword;
                    var ar = [];
                    this.data.forEach(e => {
                        ar.push(e);
                        jQuery(".abc").val(ar);
                        jQuery("#tags").append('<span>' + e + '</span>');
                    });
                }
            }
        )
        jQuery(function () { // DOM ready

            // ::: TAGS BOX
            var arr = [];
            jQuery("#tags input").on({
                focusout: function () {
                    var txt = this.value.replace(/[^a-z0-9\+\-\.\#]/ig, ''); // allowed characters
                    if (txt) {
                        arr.push(txt);
                        // this.keyWord = arr;
                        jQuery(".abc").val(arr)
                        if (txt) jQuery("<span/>", { text: txt.toLowerCase(), insertBefore: this });
                        this.value = "";
                    }
                },
                keyup: function (ev) {
                    // if: comma|enter (delimit more keyCodes with | pipe)
                    // if (/(188|13)/.test(ev.which)) jQuery(this).focusout();
                    if (/(188)/.test(ev.which)) jQuery(this).focusout();
                }
            });
            jQuery('#tags').on('click', 'span', function () {
                let indexx = arr.indexOf(jQuery(this).text());
                arr.splice(indexx, 1);
                // this.keyWord = arr;
                console.log("arrr", arr);
                jQuery(".abc").val(arr)
                jQuery(this).remove();
            });

        });

    }
    submitForm(value) {
        var x = jQuery(".abc").val();
        this.keyWord = x.split(',');
        if (this.keyWord.length == 0) {
            swal("Please enter tag");
        } else {
            console.log("keyword", this.keyWord);
            value._value.keyword = this.keyWord;
            value._value.type = 1;
            console.log("this.keyWord", value._value);
            this._service.saveSetting(value._value).subscribe(
                res => {
                    console.log("res", res);
                    if (res.code == 200) {
                        // this.ngOnInit();
                        swal("Success!", "Save successfully!", "success");
                    } else {
                        sweetAlert("Oops...", "Something went wrong!", "error");
                    }
                }
            )
        }
    }
    seoSettingDelete() {
        var news = this;
        swal({
            title: "Are you sure?",
            text: "You will not be able to recover this Setting!",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: '#DD6B55',
            confirmButtonText: 'Yes, I am sure!',
            cancelButtonText: "No, cancel it!",
            closeOnConfirm: false,
            closeOnCancel: false
        },
            function (isConfirm) {
                if (isConfirm) {
                    news.deleteSeo();
                    swal({
                        title: 'Delete!',
                        text: 'Deleted Successfully!',
                        type: 'success'
                    });

                } else {
                    swal("Cancelled", "Your Seo is safe :)", "error");
                }
            });
    }
    deleteSeo() {
        var type = 1;
        this._service.deleteSeo(type).subscribe(
            res => {
                this.ngOnInit();
                // console.log("res", res);
            }
        )
    }
}