import { Component, ViewEncapsulation, ViewContainerRef, AfterViewInit, AfterContentInit } from '@angular/core';
import { AppConfig } from "../../../app.config";
import { Router, ActivatedRoute } from '@angular/router';
import { PushMarketingService } from '../pushMarketing.service';
import { Modal } from 'angular2-modal/plugins/bootstrap';
import { ModalModule } from "ng2-modal";
import { FormGroup, FormBuilder, Validators, AbstractControl, FormControl } from '@angular/forms';
@Component({
    selector: 'targetedUser',
    encapsulation: ViewEncapsulation.None,
    styleUrls: ['./targetedUser.component.scss'],
    templateUrl: './targetedUser.component.html',
    providers: [PushMarketingService]
})

export class TargeterduserComponent {

    public rowsOnPage = 10;
    sub: any;
    campaignId: any;
    data = [];
    msg = false;
    detailData: any;


    constructor(private route: ActivatedRoute, private _appConfig: AppConfig, private _service: PushMarketingService, private router: Router, vcRef: ViewContainerRef) { }

    ngOnInit() {
        this.sub = this.route.params.subscribe(params => {
            this.campaignId = params['id'];
        });
        this._service.getAllTargetedUser(this.campaignId).subscribe(
            res => {
                // console.log("res", res);
                if (res.data[0].targetUser && res.data[0].targetUser.length > 0) {
                    this.data = res.data[0].targetUser;
                    this.msg = false;
                } else {
                    this.data = []
                    this.msg = true;
                }
            }
        )
    }

    gotoUserDatail(user) {
        this._service.getUserDetail(user).subscribe(
            result => {
                if (result.code == 200) {
                    this.detailData = result.data;
                    jQuery('#userDetail').modal('show');
                }
            }
        )
    }
}
