//importing all the essential modules
import { AuthGuard } from '../_guards/auth.guard';
import { ModuleWithProviders } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { PagesComponent } from '../pages/pages.component';
import { AdminConfigComponent } from '../admin/adminconfig/adminconfig.component';
import { AdminPolicyComponent } from '../admin/adminpolicy/adminpolicy.component';
import { AdminNewsComponent } from '../admin/adminnews/adminnews.component';
import { AdminStoryComponent } from './adminstory/adminstory.component';
import { SendPushComponent } from './sendPush/sendPush.component';
import { newPushComponent } from './sendPush/newPush/newPush.component';
import { ManageAccessComponent } from './manageAccess/manageAccess.component';
import { addRolesComponent } from './manageAccess/addRoles/addRoles.component';
import { pushTargetedUserComponent } from './sendPush/targetedUser/targetedUser.component';
import { AppVersionComponent } from './appVersion/appVersion.component';
import { usersAppVersionComponent } from './appVersion/usersAppVersion/usersAppVersion.component';
import { FAQComponent } from './adminfaq/adminfaq.component';
import { PushMarketingComponent } from './pushMarketing/pushMarketing.component';
import { TargeterduserComponent } from './pushMarketing/targetedUser/targetedUser.component';
import { CampaignViewComponent } from './pushMarketing/campaignView/campaignView.component';
import { CampaignClickComponent } from './pushMarketing/campaignClick/campaignClick.component';
import { NewCampaignComponent } from './newCampaign/newCampaign.component';
//=============== routing for user modules ======================
//=============== exporting the route to the main app =============
export const AdminRoutes: Routes = [

  {
    path: 'app',
    component: PagesComponent,
    children: [
      {
        path: 'termsandcondition',
        component: AdminConfigComponent,
        canActivate: [AuthGuard],
        pathMatch: 'full'
      },
      {
        path: 'policy',
        component: AdminPolicyComponent,
        canActivate: [AuthGuard],
      },
      {
        path: 'help',
        component: FAQComponent,
        canActivate: [AuthGuard],
        data: {
          title: 'help'
        }
      },
      {
        path: 'news',
        component: AdminNewsComponent,
        canActivate: [AuthGuard],
      },
      {
        path: 'ourstory',
        component: AdminStoryComponent,
        canActivate: [AuthGuard],
      }, 
      {
        path: 'notification',
        component: SendPushComponent,
        canActivate: [AuthGuard],
        data: {
          title: 'Send Push'
        }
      },
      {
        path: 'notification/send-new-push',
        component: newPushComponent,
        canActivate: [AuthGuard],
        data: {
          title: 'Send New Push'
        }
      },
      {
        path: 'notification/targeted-users/:id',
        component: pushTargetedUserComponent,
        canActivate: [AuthGuard],
        data: {
          title: 'Targeted Users'
        }
      },
      {
        path: 'manage-access',
        component: ManageAccessComponent,
        canActivate: [AuthGuard],
        data: {
          title: 'Manage Access'
        }
      },
      {
        path: 'manage-access/add-roles',
        component: addRolesComponent,
        canActivate: [AuthGuard],
        data: {
          title: 'Add Roles'
        }
      },
      {
        path: 'campaign',
        component: PushMarketingComponent,
        canActivate: [AuthGuard],
        data: {
          title: 'Push Marketing'
        }
      },
      {
        path: 'campaign/targeted-user/:id',
        component: TargeterduserComponent,
        canActivate: [AuthGuard],
        data: {
          title: 'Targeted User'
        }
      },
      {
        path: 'campaign/view/:id',
        component: CampaignViewComponent,
        canActivate: [AuthGuard],
        data: {
          title: 'View'
        }
      },
      {
        path: 'campaign/click/:id',
        component: CampaignClickComponent,
        canActivate: [AuthGuard],
        data: {
          title: 'Click'
        }
      },
      {
        path: 'campaign/launch-new-campaign',
        component: NewCampaignComponent,
        canActivate: [AuthGuard],
        data: {
          title: 'New Campaign'
        }
      },
    ]
  },

];


