import { NgModule, ModuleWithProviders } from '@angular/core';
import { BrowserModule, Title } from '@angular/platform-browser';
import { HttpModule } from '@angular/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { CustomFormsModule } from 'ng2-validation'
import { RouterModule } from '@angular/router';

import {
    Ng2PaginationModule, PaginationService, PaginationControlsDirective,
    PaginationControlsComponent, PaginatePipe
} from 'ng2-pagination';
import { ModalModule } from 'angular2-modal';
import { BootstrapModalModule } from 'angular2-modal/plugins/bootstrap';

import { DataTableModule } from "angular2-datatable";

import { AgmCoreModule } from "angular2-google-maps/core";

import { AdminConfigComponent } from '../admin/adminconfig/adminconfig.component';
import { AdminPolicyComponent } from '../admin/adminpolicy/adminpolicy.component';
import { ImageCropperComponent, CropperSettings } from 'ng2-img-cropper';
import { CKEditorModule } from 'ng2-ckeditor';
import { AdminNewsComponent } from './adminnews/adminnews.component';
import { AdminStoryComponent } from './adminstory/adminstory.component';
import { Ng2CloudinaryModule } from 'ng2-cloudinary';
import { FileUploadModule } from 'ng2-file-upload';
import { MyDatePickerModule } from 'mydatepicker';
import { SendPushComponent } from './sendPush/sendPush.component';
import { newPushComponent } from './sendPush/newPush/newPush.component';
import { ManageAccessComponent } from './manageAccess/manageAccess.component';
import { addRolesComponent } from './manageAccess/addRoles/addRoles.component';
import { pushTargetedUserComponent } from './sendPush/targetedUser/targetedUser.component';
import { AppVersionComponent } from './appVersion/appVersion.component';
import { usersAppVersionComponent } from './appVersion/usersAppVersion/usersAppVersion.component';
import { FAQComponent } from './adminfaq/adminfaq.component';
import { PushMarketingComponent } from './pushMarketing/pushMarketing.component';
import { TargeterduserComponent } from './pushMarketing/targetedUser/targetedUser.component';
import { CampaignViewComponent } from './pushMarketing/campaignView/campaignView.component';
import { CampaignClickComponent } from './pushMarketing/campaignClick/campaignClick.component';
import { NewCampaignComponent } from './newCampaign/newCampaign.component';
@NgModule({
    imports: [
        BrowserModule,
        HttpModule,
        RouterModule,
        FormsModule,
        ReactiveFormsModule,
        Ng2PaginationModule,
        Ng2CloudinaryModule,
        FileUploadModule,
        CKEditorModule,
        MyDatePickerModule
    ],
    declarations: [
        AdminConfigComponent,
        AdminPolicyComponent,
        AdminNewsComponent,
        AdminStoryComponent,
        ImageCropperComponent, 
        SendPushComponent,
        newPushComponent,
        ManageAccessComponent,
        addRolesComponent,
        pushTargetedUserComponent,
        AppVersionComponent,
        usersAppVersionComponent,
        FAQComponent,
        PushMarketingComponent,
        TargeterduserComponent,
        CampaignViewComponent,
        CampaignClickComponent,
        NewCampaignComponent

    ],
    providers: [],
    exports: [
    ]

})
export class AdminModule {

}