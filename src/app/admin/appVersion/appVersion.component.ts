import { Component, ViewEncapsulation, ViewContainerRef, AfterViewInit, AfterContentInit } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { AppConfig } from "../../app.config";
import { Modal } from 'angular2-modal/plugins/bootstrap';
import { ModalModule } from "ng2-modal";
import { FormGroup, FormBuilder, Validators, AbstractControl, FormControl } from '@angular/forms';
import { AppVersionService } from './appVersion.service';
import { PagesComponent } from '../../pages/pages.component';


declare var swal: any;
declare var sweetAlert: any;
@Component({
    selector: 'appVersion',
    encapsulation: ViewEncapsulation.None,
    styleUrls: ['./appVersion.component.scss'],
    templateUrl: './appVersion.component.html',
    providers: [AppVersionService]
})

export class AppVersionComponent {
    public rowsOnPage = 10;
    public p = 1;
    deviceType = '2';
    versionForm: FormGroup;
    msg = true;
    data = [];
    btnFlag = true;

    constructor(private route: ActivatedRoute, private _service: AppVersionService, private router: Router, vcRef: ViewContainerRef, fb: FormBuilder, public _isAdmin: PagesComponent) {
        this.versionForm = fb.group({
            'version': ['', Validators.required],
        });
    }
    ngOnInit() {
        if (this._isAdmin.isAdmin == false) {
            var role = sessionStorage.getItem('role');
            var roleDt = JSON.parse(role);
            for (var x in roleDt) {
                if (x == 'app-version') {
                    if (roleDt[x] == 0) {
                        this.router.navigate(['error']);
                    } else if (roleDt[x] == 100) {
                        jQuery(".buttonCus").hide();
                    }
                }
            }
        }

        this._service.getAllVersion(this.deviceType).subscribe(
            res => {
                if (res.data && res.data.length > 0) {
                    this.msg = false;
                    this.data = res.data;
                } else {
                    this.msg = true;
                    this.data = [];
                }
            }
        )
    }

    submitForm(val) {
        var mandatory = jQuery("#mandatory").is(":checked");
        val._value.mandatory = mandatory;
        val._value.deviceType = this.deviceType;
        this._service.saveAppVersion(val._value).subscribe(
            res => {
                if (res.code == 200) {
                    swal("Success!", "Successfully added!", "success");
                    this.versionForm.reset();
                    jQuery('#addNew').modal('hide');
                    this.ngOnInit();
                } else {
                    sweetAlert("Oops...", "Something went wrong!", "error");
                }
            }
        )
    }
    osType(val) {
        this.deviceType = val;
        this._service.getAllVersion(this.deviceType).subscribe(
            res => {
                // console.log("res", res);
                if (res.data && res.data.length > 0) {
                    this.msg = false;
                    this.data = res.data;
                } else {
                    this.msg = true;
                    this.data = [];
                }
            }
        )
    }
    gotoUser(type, version) {
        this.router.navigate(['/app/app-version/users', type, version]);
    }
}