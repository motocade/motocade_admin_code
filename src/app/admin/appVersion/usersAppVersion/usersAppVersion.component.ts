import { Component, ViewEncapsulation, ViewContainerRef, AfterViewInit, AfterContentInit } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { AppConfig } from "../../../app.config";
import { Modal } from 'angular2-modal/plugins/bootstrap';
import { ModalModule } from "ng2-modal";
import { FormGroup, FormBuilder, Validators, AbstractControl, FormControl } from '@angular/forms';
import { AppVersionService } from './../appVersion.service';
import { PagesComponent } from '../../../pages/pages.component';


declare var swal: any;
declare var sweetAlert: any;
@Component({
    selector: 'usersAppVersion',
    encapsulation: ViewEncapsulation.None,
    styleUrls: ['./usersAppVersion.component.scss'],
    templateUrl: './usersAppVersion.component.html',
    providers: [AppVersionService]
})

export class usersAppVersionComponent {
    public rowsOnPage = 10;
    msg = true;
    data = [];
    sub: any;
    type: any;
    version: any;
    constructor(private route: ActivatedRoute, private _service: AppVersionService, private router: Router, vcRef: ViewContainerRef, fb: FormBuilder, public _isAdmin: PagesComponent) { }
    ngOnInit() {
        if (this._isAdmin.isAdmin == false) {
            var role = sessionStorage.getItem('role');
            var roleDt = JSON.parse(role);
            for (var x in roleDt) {
                if (x == 'app-version') {
                    if (roleDt[x] == 0) {
                        this.router.navigate(['error']);
                    } else if (roleDt[x] == 100) {
                        jQuery(".buttonCus").hide();
                    }
                }
            }
        }
        this.sub = this.route.params.subscribe(params => {
            this.type = params['type'];
            this.version = params['version']
        });


        this._service.getAllUsers(this.type, this.version).subscribe(
            res => {
                // console.log("res", res);
                if (res.data && res.data.length > 0) {
                    this.msg = false;
                    this.data = res.data;
                } else {
                    this.msg = true;
                    this.data = [];
                }
            }
        )
    }

}