import { Component, ViewEncapsulation, ViewContainerRef, AfterViewInit, AfterContentInit } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { AppConfig } from "../../app.config";
import { Modal } from 'angular2-modal/plugins/bootstrap';
import { ModalModule } from "ng2-modal";
import { FormGroup, FormBuilder, Validators, AbstractControl, FormControl } from '@angular/forms';
import { AccountsService } from './accounts.service';


declare var swal: any;
declare var sweetAlert: any;
@Component({
    selector: 'accounts',
    encapsulation: ViewEncapsulation.None,
    styleUrls: ['./accounts.component.scss'],
    templateUrl: './accounts.component.html',
    providers: [AccountsService]
})

export class AccountsComponent {
    accSetting: FormGroup;
    type = 2;
    data = {};
    constructor(private route: ActivatedRoute, private _service: AccountsService, private router: Router, vcRef: ViewContainerRef, fb: FormBuilder) {
        this.accSetting = fb.group({
            'facebook': ['', Validators.required],
            'twitter': ['', Validators.required],
            'instagram': ['', Validators.required],
            'linkedin': ['', Validators.required],
            'pinterest': ['', Validators.required],
            'youtube': ['', Validators.required],
            'google': ['', Validators.required],
        });
    }
    ngOnInit() {
        var type = this.type;
        this._service.getSEOSetting(type).subscribe(
            res => {
                console.log("res", res);
                if (res.data) {
                    this.data = res.data;
                }

            }
        )
    }
    submitForm(value) {
        value._value.type = this.type;
        // console.log("this.keyWord", value._value);
        this._service.saveSetting(value._value).subscribe(
            res => {
                console.log("res", res);
                if (res.code == 200) {
                    // this.ngOnInit();
                    swal("Success!", "Save successfully!", "success");
                } else {
                    sweetAlert("Oops...", "Something went wrong!", "error");
                }
            }
        )
    }
}